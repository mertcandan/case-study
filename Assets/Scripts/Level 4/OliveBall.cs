﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OliveBall : MonoBehaviour
{
    // TODO: collision problems
    
    public float moveSpeed = 5f;
    public BricksController bricksController;
    public float wobbleAmount;
    
    private bool _moving;
    private float _wobbleTimer;
    private float _wobbleOn = 1f;

    void Start()
    {
        _moving = false;
        
        GameManager.OnGameStarted += EnableMovement;
        GameManager.OnGamePaused += DisableMovement;
        GameManager.OnGameResumed += EnableMovement;
        GameManager.OnGameEnded += DisableMovement;
    }

    void Update()
    {
        Move();
    }
    
    private void OnDisable()
    {
        GameManager.OnGameStarted -= EnableMovement;
        GameManager.OnGamePaused -= DisableMovement;
        GameManager.OnGameResumed -= EnableMovement;
        GameManager.OnGameEnded -= DisableMovement;
    }

    void EnableMovement()
    {
        _moving = true;
    }

    void DisableMovement()
    {
        _moving = false;
    }

    void Move()
    {
        if (!_moving)
        {
            return;
        }
        
        transform.position =
            transform.position +
            moveSpeed * Time.deltaTime * transform.forward +
            wobbleAmount * (Mathf.Sin(5f * _wobbleTimer + Time.deltaTime) - Mathf.Sin(5f * _wobbleTimer)) * transform.right;

        _wobbleTimer += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.FAIL_AREA))
        {
            GameManager.Instance.RestartGame();
            return;
        }
        
        ChangeDirection(other);
        if (other.CompareTag(Tags.BRICK))
        {
            Brick brick = other.GetComponent<Brick>();
            if (!brick.collided)
            {
                brick.OnCollidedWithBall(this, bricksController.playerPad);
                bricksController.BrickCollected();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _wobbleOn = 1f;
    }

    /// <summary>
    /// Raycast and reflect the ball off the surface of the object the ball has collided.
    /// </summary>
    void ChangeDirection(Collider other)
    {
        _wobbleOn = 0f;
        
        RaycastHit hit;
        if (other.Raycast(new Ray(transform.position, transform.forward),
            out hit, 1f))
        {
            Vector3 reflected = Vector3.Reflect(
                transform.forward, hit.normal);

            
            Debug.DrawRay(hit.point, - transform.forward, Color.green, 2f);
            transform.forward = new Vector3(
                reflected.x,
                0,
                reflected.z);
            
            
            Debug.DrawRay(hit.point, hit.normal, Color.red, 2f);
            Debug.DrawRay(hit.point, reflected, Color.blue , 2f);
        }
        else
        {
            // try edges
            var leftEdge = transform.position 
                           - 0.1f * transform.right * transform.localScale.x
                           - transform.forward;
            
            var rightEdge = transform.position
                            + 0.1f * transform.right * transform.localScale.x 
                            - transform.forward;
            
            if (other.Raycast(new Ray(leftEdge, transform.forward),
                out hit, 2f))
            {
                Vector3 reflected = Vector3.Reflect(
                    transform.forward, hit.normal);

                transform.forward = new Vector3(
                    reflected.x,
                    0,
                    reflected.z);
            
                // Debug.DrawRay(hit.point, hit.normal, Color.red, 2f);
                // Debug.DrawRay(hit.point, reflected, Color.blue , 2f);
            }
            else if (other.Raycast(new Ray(rightEdge, transform.forward),
                out hit, 2f))
            {
                Vector3 reflected = Vector3.Reflect(
                    transform.forward, hit.normal);

                transform.forward = new Vector3(
                    reflected.x,
                    0,
                    reflected.z);
            
                // Debug.DrawRay(hit.point, hit.normal, Color.red, 2f);
                // Debug.DrawRay(hit.point, reflected, Color.blue , 2f);
            }
            else
            {
                // issue!
                Debug.LogError("No hit!");
                Debug.DrawRay(leftEdge, transform.forward, Color.red, 2f);
                Debug.DrawRay(rightEdge, transform.forward, Color.blue , 2f);
            }
        }
    }
}
