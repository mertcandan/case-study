﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public bool collided;
    private BrickAction[] _brickActions;
    
    private void Start()
    {
        _brickActions = GetComponents<BrickAction>();
    }

    public void OnCollidedWithBall(OliveBall oliveBall, PlayerPad playerPad)
    {
        if (!collided)
        {
            collided = true;
            foreach (BrickAction brickAction in _brickActions)
            {
                brickAction.PerformAction(oliveBall, playerPad);
            }
            Destroy(gameObject);
        }
    }
}
