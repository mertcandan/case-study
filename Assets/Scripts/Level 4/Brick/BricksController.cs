﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BricksController : MonoBehaviour
{
    public Brick[] brickPrefabs;
    
    public Transform spawnTopLeft;
    public Transform spawnTopRight;
    public PlayerPad playerPad;
    
    public int rowCount = 5;
    public int columnCount = 5;

    private int _collectedCount;
    private List<int> _randomBricks;
    
    void Start()
    {
        SpawnBricks();
        GameManager.Instance.SetTotalLevelSteps(rowCount * columnCount);
    }

    void SpawnBricks()
    {
        CreateRandomBricks();
        for (int i = 0; i < rowCount; i++)
        {
            for (int j = 0; j < columnCount; j++)
            {
                var position = CalculatePosition(i, j);
                Instantiate(
                    brickPrefabs[_randomBricks[i * columnCount + j]],
                    CalculatePosition(i, j),
                    Quaternion.identity
                );
            }
        }
    }

    /// <summary>
    /// try to balance the number of each type of brick
    /// </summary>
    void CreateRandomBricks()
    {
        _randomBricks = new List<int>();

        int brickTypeCount = rowCount * columnCount / brickPrefabs.Length;
        for (int i = 0; i < brickPrefabs.Length; i++)
        {
            for (int j = 0; j < brickTypeCount; j++)
            {
                _randomBricks.Add(i);
            }
        }

        while (_randomBricks.Count < rowCount * columnCount)
        {
            _randomBricks.Add(Random.Range(0, brickPrefabs.Length));
        }
        
        var rnd = new System.Random();
        _randomBricks = _randomBricks.OrderBy(item => rnd.Next()).ToList();
    }

    Vector3 CalculatePosition(int i, int j)
    {
        var topLeft = spawnTopLeft.position;
        var bottomRight = spawnTopRight.position;
        var xDist = (bottomRight.x - topLeft.x) / (columnCount - 1);
        var zDist = (bottomRight.z - topLeft.z) / (rowCount - 1);

        return topLeft + new Vector3(
            xDist * j,
            0,
            zDist * i);
    }

    public void BrickCollected()
    {
        _collectedCount++;
        GameManager.Instance.LevelProgressed();
        if (_collectedCount == rowCount * columnCount)
        {
            GameManager.Instance.EndGame();
        }
    }
}
