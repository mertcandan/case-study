﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpeedUp : BrickAction
{
    public float ballSpeedIncrement = 0.5f;
    public float maxBallSpeed = 10f;
    public override void PerformAction(OliveBall oliveBall, PlayerPad playerPad)
    {
        oliveBall.moveSpeed = Mathf.Min(
            oliveBall.moveSpeed + ballSpeedIncrement,
            maxBallSpeed);
    }
}
