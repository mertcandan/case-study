﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadLengthUp : BrickAction
{
    public float padLengthIncrement = 0.5f;
    public float maxPadLength = 5f;
    public override void PerformAction(OliveBall oliveBall, PlayerPad playerPad)
    {
        var playerPadTransform = playerPad.transform;
        var padScale = playerPadTransform.localScale;
        var size = Mathf.Min(padScale.x + padLengthIncrement, maxPadLength);
        var clampedPadScale = new Vector3(size, size, size);

        playerPad.SetPadSize(clampedPadScale);
    }
}
