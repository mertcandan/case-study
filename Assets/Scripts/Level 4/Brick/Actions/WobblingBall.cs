﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WobblingBall : BrickAction
{
    public float wobbleIncrement = 0.5f;
    public float maxWobble = 5f;
    
    public override void PerformAction(OliveBall oliveBall, PlayerPad playerPad)
    {
        oliveBall.wobbleAmount = Mathf.Clamp(
            oliveBall.wobbleAmount + wobbleIncrement,
            2f,
            maxWobble);
    }
}
