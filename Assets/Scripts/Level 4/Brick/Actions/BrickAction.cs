﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BrickAction : MonoBehaviour
{
    public abstract void PerformAction(OliveBall oliveBall, PlayerPad playerPad);
}
