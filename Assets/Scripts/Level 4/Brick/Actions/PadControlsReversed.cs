﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PadControlsReversed : BrickAction
{
    public override void PerformAction(OliveBall oliveBall, PlayerPad playerPad)
    {
        playerPad.ReverseDirection();
    }
}
