﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiggerBall : BrickAction
{
    public float sizeIncrement = 0.5f;
    public float maxSize = 5f;
    
    public override void PerformAction(OliveBall oliveBall, PlayerPad playerPad)
    {
        var oliveBallTransform = oliveBall.transform;
        var oliveBallScale = oliveBallTransform.localScale;
        var newBallScale = new Vector3(
            Mathf.Min(oliveBallScale.x + sizeIncrement, maxSize),
            Mathf.Min(oliveBallScale.y + sizeIncrement, maxSize),
            Mathf.Min(oliveBallScale.z + sizeIncrement, maxSize));
        
        oliveBallTransform.localScale = newBallScale;
    }
}
