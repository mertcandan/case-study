﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPad : MonoBehaviour
{
    public BoundaryScriptableObject boundarySO;
    
    public float speed;
    
    private bool _canMove;
    private float _leftLimit;
    private float _rightLimit;

    private float _lastInputX;
    private float _currentInputX;
    
    private float _direction = 1f;
    
    void Start()
    {
        _canMove = false;
        CalculateLimits();

        GameManager.OnGameStarted += EnableMovement;
        GameManager.OnGameEnded += DisableMovement;
    }

    void CalculateLimits()
    {
        _leftLimit = boundarySO.restrictedAreaWidth * -0.5f
                     + boundarySO.wallThickness * 0.5f
                     + transform.localScale.x * 0.25f;

        _rightLimit = boundarySO.restrictedAreaWidth * 0.5f
                      - boundarySO.wallThickness * 0.5f
                      - transform.localScale.x * 0.25f;
    }
    
    private void OnDisable()
    {
        GameManager.OnGameStarted -= EnableMovement;
        GameManager.OnGameEnded -= DisableMovement;
    }
    
    void EnableMovement()
    {
        _canMove = true;
    }

    void DisableMovement()
    {
        _canMove = false;
    }

    void Update()
    {
        GetInput();
    }
    
    #region Input Processing

    void GetInput()
    {
        if (!_canMove)
        {
            return;
        }
        
        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _lastInputX = Input.mousePosition.x;
            }
            else if (Input.GetMouseButton(0))
            {
                _currentInputX = Input.mousePosition.x;
                MovePad();
                _lastInputX = Input.mousePosition.x;
            }
        }
        else
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        _lastInputX = touch.position.x;
                        break;
                    
                    case TouchPhase.Moved:
                        _currentInputX = touch.position.x;
                        MovePad();
                        _lastInputX = touch.position.x;
                        break;
                }
            }
        }
    }

    #endregion
    
    #region Movement

    void MovePad()
    {
        float diff = (_currentInputX - _lastInputX) / Screen.width;
        float displacement = diff * speed * _direction;
        float xPosition = Mathf.Clamp(transform.position.x + displacement, _leftLimit, _rightLimit);
        transform.position = new Vector3(xPosition, transform.position.y, transform.position.z);
    }
    
    #endregion

    public void ReverseDirection()
    {
        _direction *= -1f;
    }

    public void SetPadSize(Vector3 scale)
    {
        transform.localScale = scale;
        CalculateLimits();
    }
}
