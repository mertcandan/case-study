﻿using UnityEngine;

[CreateAssetMenu(fileName = "Boundary", menuName = "ScriptableObjects/Level1/Boundary", order = 1)]
public class BoundaryScriptableObject : ScriptableObject
{
    public float restrictedAreaWidth = 5.5f;
    public float restrictedAreaHeight = 9f;

    public float wallThickness = 0.3f;
}