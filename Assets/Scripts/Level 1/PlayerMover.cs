﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour
{
    public LayerMask floorLayer;
    public BoundaryScriptableObject boundarySO;

    public float moveSpeed = 3f;
    public float turnAngle = 300f;
    public float targetReachThreshold = 0.5f;
    
    private Camera _mainCamera;
    private bool _canMove;
    private bool _moving;
    private Vector3 _target;

    // Start is called before the first frame update
    void Start()
    {
        _moving = false;
        _canMove = false;
        _mainCamera = Camera.main;

        GameManager.OnGameStarted += EnableMovement;
        GameManager.OnGameEnded += DisableMovement;
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        Move();
    }

    private void OnDisable()
    {
        GameManager.OnGameStarted -= EnableMovement;
        GameManager.OnGameEnded -= DisableMovement;
    }

    void EnableMovement()
    {
        _canMove = true;
    }

    void DisableMovement()
    {
        _canMove = false;
    }
    
    #region Input Processing

    void GetInput()
    {
        if (!_canMove)
        {
            return;
        }
        
        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ProcessTarget(Input.mousePosition);
            }
        }
        else
        {
            if (Input.touchCount > 0)
            {
                ProcessTarget(Input.GetTouch(0).position);
            }
        }
    }

    void ProcessTarget(Vector3 screenPosition)
    {
        Ray ray = _mainCamera.ScreenPointToRay(screenPosition);
        
        if (Physics.Raycast(ray, out var hit, 100f, floorLayer)) {
            if (InsideBoundary(hit.point))
            {
                SetTarget(hit.point);
            }
        }
    }

    void SetTarget(Vector3 point)
    {
        _target = point;
        _moving = true;
    }
    
    #endregion
    
    #region Movement

    void Move()
    {
        if (!_moving)
        {
            return;
        }
        
        ChangeDirection();
        MoveForward();
        CheckReached();
    }

    void ChangeDirection()
    {
        var direction = _target - transform.position;
        transform.rotation = Quaternion.RotateTowards(
            transform.rotation,
            Quaternion.LookRotation(direction, Vector3.up),
            turnAngle * Time.deltaTime);
    }

    void MoveForward()
    {
        // transform.Translate((moveSpeed * Time.deltaTime) * 
        transform.position = transform.position + (moveSpeed * Time.deltaTime) * transform.forward;
    }

    void CheckReached()
    {
        if (Vector3.Distance(transform.position, _target) < targetReachThreshold)
        {
            _moving = false;
        }
    }
    
    #endregion

    #region Utils
    
    bool InsideBoundary(Vector3 position)
    {
        return position.x > boundarySO.restrictedAreaWidth / -2f &&
               position.x < boundarySO.restrictedAreaWidth / 2f &&
               position.z > boundarySO.restrictedAreaHeight / -2f &&
               position.z < boundarySO.restrictedAreaHeight / 2f;
    }
    
    #endregion
}
