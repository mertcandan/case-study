﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OliveCollector : MonoBehaviour
{
    public int totalOliveCount = 64;
    public float collectionMoveDuration = 0.5f;
    
    private int _collectedOliveCount;
    private List<Vector3> _collectionPositions;

    private void Start()
    {
        CalculateSpawnPositions();
    }

    private void Update()
    {
        CheckAllOlivesCollected();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Tags.OLIVE))
        {
            CollectOlive(other.gameObject);
        }
    }

    void CheckAllOlivesCollected()
    {
        if (_collectedOliveCount == totalOliveCount)
        {
            GameManager.Instance.EndGame();
        }
    }

    void CollectOlive(GameObject olive)
    {
        _collectedOliveCount++;
        SphereCollider oliveCollider = olive.GetComponent<SphereCollider>();
        oliveCollider.enabled = false;
        // TODO: check
        olive.GetComponent<Rigidbody>().isKinematic = true;
        StartCoroutine(Collecting(
            olive.transform, 
            transform.position +
            _collectionPositions[_collectedOliveCount - 1]));
        
        GameManager.Instance.LevelProgressed();
    }

    IEnumerator Collecting(Transform olive, Vector3 destination)
    {
        float elapsed = 0;
        Vector3 startPosition = olive.position;
        
        while (elapsed < collectionMoveDuration)
        {
            elapsed += Time.deltaTime;
            olive.position = Vector3.Lerp(
                startPosition,
                destination,
                elapsed / collectionMoveDuration);
            yield return new WaitForEndOfFrame();

        }

        olive.position = destination;
    }

    void CalculateSpawnPositions()
    {
        _collectionPositions = new List<Vector3>();
    
        _collectionPositions.Add(Vector3.zero);

        int level = 1;
        float height = 0;
        while (_collectionPositions.Count < totalOliveCount - 1)
        {
            float radius = 0.20f * level;
            int count = (int)(2 * Math.PI * level);
            float slice = (float)(2 * Math.PI / count);
            for (int i = 0; i < count; i++)
            {
                if (_collectionPositions.Count < totalOliveCount - 1)
                {
                    float angle = slice * i;
                    float xPos = (float)(radius * Math.Cos(angle));
                    float zPos = (float)(radius * Math.Sin(angle));
                    _collectionPositions.Add(new Vector3(xPos, height, zPos));
                    if (_collectionPositions.Count == 37 || _collectionPositions.Count == 56)
                    {
                        level = 0;
                        height += 0.1f;
                        _collectionPositions.Add(new Vector3(0, height, 0));
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            level++;
        }
        _collectionPositions.Add(new Vector3(0, height + 0.1f, 0));
    }
}
