﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OliveSpawner : MonoBehaviour
{
    public GameObject olivePrefab;
    public int spawnCount = 64;
    public bool shouldSpawnApart;
    public BoundaryScriptableObject boundarySO;

    private List<Vector3> _spawnedPositions;
    
    void Start()
    {
        if (shouldSpawnApart)
        {
            _spawnedPositions = new List<Vector3>();
        }
        SpawnOlives();
        GameManager.Instance.SetTotalLevelSteps(spawnCount);
    }

    void SpawnOlives()
    {
        for (int i = 0; i < spawnCount; i++)
        {
            Vector3 randomPosition = GetRandomPosition();
            if (shouldSpawnApart)
            {
                randomPosition = GetValidRandomPosition(randomPosition);
                _spawnedPositions.Add(randomPosition);
            }
            Instantiate(
                olivePrefab,
                randomPosition,
                Quaternion.identity);
        }
    }

    Vector3 GetRandomPosition()
    {
        float randX = Random.Range(
            boundarySO.restrictedAreaWidth / -2f, 
            boundarySO.restrictedAreaWidth / 2f);
        
        float randZ = Random.Range(
            boundarySO.restrictedAreaHeight / -2f,
            boundarySO.restrictedAreaHeight / 2f);

        return new Vector3(
            randX,
            0,
            randZ);
    }

    Vector3 GetValidRandomPosition(Vector3 initial)
    {
        Vector3 testing = initial;

        while (PositionTooClose(testing))
        {
            testing = GetRandomPosition();
        }

        return testing;
    }

    bool PositionTooClose(Vector3 testing)
    {
        foreach (Vector3 spawnedPosition in _spawnedPositions)
        {
            if (Vector3.Distance(spawnedPosition, testing) < 0.7f)
            {
                return true;
            }
        }

        return false;
    }
}
