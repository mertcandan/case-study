﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject startPanel;
    public GameObject inGamePanel;
    public GameObject endPanel;
    public GameObject inventoryPanel;
    public GameObject inventoryModels;
    public TMP_Text[] collectibleCountTexts;
    
    public Image progressFill;
    
    private void Start()
    {
        GameManager.OnGameStarted += GameStarted;
        GameManager.OnGameEnded += GameEnded;
        GameManager.OnLevelProgress += LevelProgressed;
    }

    private void OnDisable()
    {
        GameManager.OnGameStarted -= GameStarted;
        GameManager.OnGameEnded -= GameEnded;
        GameManager.OnLevelProgress -= LevelProgressed;
    }

    #region Buttons
    
    public void OnStartPressed()
    {
        GameManager.Instance.StartGame();
    }
    
    public void OnRestartPressed()
    {
        GameManager.Instance.RestartGame();
    }

    public void OnNextPressed()
    {
        GameManager.Instance.PerformLevelTransition();
    }

    public void OnInventoryPressed()
    {
        inventoryPanel.SetActive(true);
        inventoryModels.SetActive(true);
        SetCollectibleCounts();
        GameManager.Instance.PauseGame();
    }

    public void OnCloseInventoryPressed()
    {
        inventoryPanel.SetActive(false);
        inventoryModels.SetActive(false);
        GameManager.Instance.ResumeGame();
    }
    
    #endregion
    
    void GameEnded()
    {
        inGamePanel.SetActive(false);
        endPanel.SetActive(true);
    }
    
    void GameStarted()
    {
        startPanel.SetActive(false);
        inGamePanel.SetActive(true);
    }

    void LevelProgressed(float progress)
    {
        progressFill.fillAmount = progress;
    }

    void SetCollectibleCounts()
    {
        int[] counts = GameManager.Instance.GetCollectibleCounts();
        for (int i = 0; i < counts.Length; i++)
        {
            collectibleCountTexts[i].text = counts[i].ToString();
        }
    }
}
