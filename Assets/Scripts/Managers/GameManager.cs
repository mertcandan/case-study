﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    // TODO: remove
    public bool testing;
    public LevelOrderScriptableObject levelOrderSO;

    public bool loadUserData;
    public bool giftRandomCollectible;
    
    public int levelIndex;
    public List<int> collected;

    public CollectibleShowcase[] collectibles;
    public Camera collectibleCamera;
    public Transform collectiblePosition;
    public TMP_Text[] collectedItemCountTexts;
    public Transform[] collectedDestinations;
    
    public static GameManager Instance;

    private bool _gameRunning;
    private float _totalLevelSteps;
    private float _levelStepsTaken;
    
    public delegate void GameStartAction();
    public delegate void GamePauseAction();
    public delegate void GameResumeAction();
    public delegate void GameEndAction();
    public delegate void LevelProgressAction(float progress);

    public static event LevelProgressAction OnLevelProgress;
    
    public static event GameStartAction OnGameStarted;
    public static event GamePauseAction OnGamePaused;
    public static event GameResumeAction OnGameResumed;
    public static event GameEndAction OnGameEnded;
    
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        if (loadUserData)
        {
            LoadPlayerData();
        }
        
        if (!testing)
            SceneManager.LoadScene(levelOrderSO.order[levelIndex]);
    }
    
    #region Game Events

    public void StartGame()
    {
        OnGameStarted?.Invoke();
        _gameRunning = true;
    }

    public void PauseGame()
    {
        OnGamePaused?.Invoke();
    }

    public void ResumeGame()
    {
        OnGameResumed?.Invoke();
    }
    
    public void EndGame()
    {
        if (!_gameRunning)
        {
            return;
        }

        _gameRunning = false;
        OnGameEnded?.Invoke();
    }

    public void RestartGame()
    {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(sceneIndex);
    }
    
    #endregion
    
    #region Level Progress

    public void SetTotalLevelSteps(float total)
    {
        _levelStepsTaken = 0;
        _totalLevelSteps = total;
    }

    public void LevelProgressed(float progressAmount = 1f)
    {
        _levelStepsTaken += progressAmount;
        float progress = _levelStepsTaken / _totalLevelSteps;
        OnLevelProgress?.Invoke(progress);
    }
    
    #endregion
    
    #region Level Transition

    public void PerformLevelTransition()
    {
        if (giftRandomCollectible)
        {
            StartCoroutine(CollectingRandom());
        }
        else
        {
            LoadNextLevel();
        }
    }

    void LoadNextLevel()
    {
        if (giftRandomCollectible)
        {
            collectibleCamera.gameObject.SetActive(false);
        }

        levelIndex++;
        if (levelIndex == levelOrderSO.order.Length)
        {
            levelIndex = 0;
            SceneManager.LoadScene(levelOrderSO.order[levelIndex]);
        }
        else
        {
            SceneManager.LoadScene(levelOrderSO.order[levelIndex]);
        }
    }
    
    #endregion
    
    private void OnApplicationQuit()
    {
        SavePlayerData();
    }
    
    #region Collectible

    IEnumerator CollectingRandom()
    {
        CollectRandom();
        yield return new WaitForSeconds(6f);
        LoadNextLevel();
    }

    void CollectRandom()
    {
        var collectibleIndex = Random.Range(0, collectibles.Length);
        CollectibleShowcase randomCollectible = collectibles[collectibleIndex];
        collectibleCamera.gameObject.SetActive(true);
        int[] counts = GetCollectibleCounts();
        for (int i = 0; i < collectedItemCountTexts.Length; i++)
        {
            collectedItemCountTexts[i].text = $"x{counts[i]}";
        }
        
        CollectibleShowcase item = Instantiate(
            randomCollectible,
            collectiblePosition.position,
            collectiblePosition.rotation);
        item.destination = collectedDestinations[collectibleIndex];
        item.destinationText = collectedItemCountTexts[collectibleIndex];
        item.destinationCount = counts[collectibleIndex] + 1;
        
        collected.Add(collectibleIndex);
    }

    public int[] GetCollectibleCounts()
    {
        int[] counts = new int[collectibles.Length];
        foreach (int collectedId in collected)
        {
            counts[collectedId]++;
        }

        return counts;
    }
    
    #endregion
    
    #region Save/Load User Data

    void SavePlayerData()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.data";
        FileStream stream = new FileStream(path, FileMode.Create);

        PlayerData data = new PlayerData(levelIndex, collected);

        formatter.Serialize(stream, data);
        stream.Close();
        Debug.Log("Saved Player Data");
    }

    void LoadPlayerData()
    {
        Debug.Log("Loading Player Data from " + Application.persistentDataPath + "/player.data");
        string path = Application.persistentDataPath + "/player.data";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            PlayerData data = formatter.Deserialize(stream) as PlayerData;
            stream.Close();

            if (data != null)
            {
                levelIndex = data.levelIndex;
                collected = data.collected;
            }
            else
            {
                Debug.Log("Player Data null");
            }
        }
        else
        {
            Debug.Log("Save file not found in " + path);
        }
    }
    
    #endregion
}
