﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct MatrixPosition
{
    public int Row;
    public int Column;

    public MatrixPosition(int row, int column)
    {
        Row = row;
        Column = column;
    }
}

public class Fruit : MonoBehaviour
{
    private MatrixPosition _matrixPosition;

    public void SetMatrixPosition(int row, int column)
    {
        _matrixPosition.Row = row;
        _matrixPosition.Column = column;
    }

    public void SetMatrixPosition(MatrixPosition mp)
    {
        _matrixPosition = mp;
    }
    
    public MatrixPosition GetMatrixPosition()
    {
        return _matrixPosition;
    }

    public void MoveTo(Vector3 destination, float duration)
    {
        StartCoroutine(Swapping(destination, duration));
    }

    IEnumerator Swapping(Vector3 destination, float duration)
    {
        float elapsed = 0;
        Vector3 initialPosition = transform.position;
        
        while (elapsed < duration)
        {
            elapsed += Time.deltaTime;
            transform.position = Vector3.Lerp(
                initialPosition,
                destination,
                elapsed / duration);
            yield return new WaitForEndOfFrame();
        }

        transform.position = destination;
    }
}
