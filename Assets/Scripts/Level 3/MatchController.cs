﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Matrix
{
    public int[] _matrix;

    public Matrix(int[] matrix)
    {
        _matrix = matrix;
    }
}

public class MatchController : MonoBehaviour
{
    public Fruit[] prefabs;
    public BoundaryScriptableObject boundarySO;
    
    public int rows = 6;
    public int columns = 5;
    public LayerMask selectableLayer;
    
    private int[,] _matrix;
    private Fruit[,] _fruits;
    private bool _canSelect;
    private Camera _mainCamera;
    private bool _firstSelected;
    private MatrixPosition _firstMP;
    private MatrixPosition _secondMP;
    
    private void Start()
    {
        _mainCamera = Camera.main;
        GameManager.OnGameStarted += EnableSelection;
        GameManager.OnGameEnded += DisableSelection;
        
        ReadMatrix();
        GameManager.Instance.SetTotalLevelSteps(rows * columns);
    }

    private void Update()
    {
        GetInput();
    }
    
    private void OnDisable()
    {
        GameManager.OnGameStarted -= EnableSelection;
        GameManager.OnGameEnded -= DisableSelection;
    }

    void EnableSelection()
    {
        _canSelect = true;
    }

    void DisableSelection()
    {
        _canSelect = false;
    }

    void ReadMatrix()
    {
        TextAsset matrixFile = Resources.Load<TextAsset>("matrix");
        Matrix matrixJson = JsonUtility.FromJson<Matrix>(matrixFile.text);

        _matrix = new int[rows, columns];
        _fruits = new Fruit[rows, columns];

        for (int i = 0; i < matrixJson._matrix.Length; i++)
        {
            _matrix[i / columns, i % columns] = matrixJson._matrix[i];
        }

        for (int i = 0; i < _matrix.GetLength(0); i++)
        {
            for (int j = 0; j < _matrix.GetLength(1); j++)
            {
                Vector3 position = GetPosition(i, j);
                _fruits[i, j] = Instantiate(
                    prefabs[_matrix[i, j]],
                    position,
                    Quaternion.identity);
                _fruits[i, j].SetMatrixPosition(i, j);
            }
        }
    }

    Vector3 GetPosition(int row, int column)
    {
        float xDistance = boundarySO.restrictedAreaWidth / columns;
        float zDistance = boundarySO.restrictedAreaHeight / rows;

        float xPosition = boundarySO.restrictedAreaWidth * -0.5f + xDistance * (0.5f + column);
        float zPosition = boundarySO.restrictedAreaHeight * 0.5f - zDistance * (0.5f + row);

        return new Vector3(xPosition, 0, zPosition);
    }

    void GetInput()
    {
        if (!_canSelect)
        {
            return;
        }
        
        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ProcessTarget(Input.mousePosition);
            }
        }
        else
        {
            if (Input.touchCount > 0)
            {
                ProcessTarget(Input.GetTouch(0).position);
            }
        }
    }

    void ProcessTarget(Vector3 screenPosition)
    {
        Ray ray = _mainCamera.ScreenPointToRay(screenPosition);
        
        if (Physics.Raycast(ray, out var hit, 100f, selectableLayer))
        {
            var matrixPosition = hit.transform.GetComponent<Fruit>().GetMatrixPosition();
            if (!_firstSelected)
            {
                _firstSelected = true;
                _firstMP = matrixPosition;
                Debug.Log("First " + matrixPosition.Row + " " + matrixPosition.Column);
            }
            else
            {
                if (CheckAdjacency(matrixPosition))
                {
                    SwapWith(matrixPosition);
                }
                else
                {
                    _firstSelected = false;
                }
                Debug.Log("Second " + matrixPosition.Row + " " + matrixPosition.Column);
            }
        }
    }

    bool CheckAdjacency(MatrixPosition matrixPosition)
    {
        int rowDiff = Mathf.Abs(_firstMP.Row - matrixPosition.Row);
        int columnDiff = Mathf.Abs(_firstMP.Column - matrixPosition.Column);

        return rowDiff + columnDiff == 1;
    }

    void SwapWith(MatrixPosition matrixPosition)
    {
        _secondMP = matrixPosition;
        SwapFruits(_firstMP, _secondMP);
        StartCoroutine(WaitForSwap(0.5f));
    }

    void SwapFruits(MatrixPosition firstMatrixPosition, MatrixPosition secondMatrixPosition)
    {
        Fruit first = GetFruit(firstMatrixPosition); 
        Fruit second = GetFruit(secondMatrixPosition);

        first.MoveTo(second.transform.position, 0.5f);
        second.MoveTo(first.transform.position, 0.5f);
        first.SetMatrixPosition(_secondMP);
        second.SetMatrixPosition(firstMatrixPosition);
        _fruits[firstMatrixPosition.Row, firstMatrixPosition.Column] = second;
        _fruits[secondMatrixPosition.Row, secondMatrixPosition.Column] = first;
        (_matrix[firstMatrixPosition.Row, firstMatrixPosition.Column], _matrix[secondMatrixPosition.Row, secondMatrixPosition.Column]) 
            = (_matrix[secondMatrixPosition.Row, secondMatrixPosition.Column], _matrix[firstMatrixPosition.Row, firstMatrixPosition.Column]);
    }

    Fruit GetFruit(MatrixPosition matrixPosition)
    {
        return _fruits[matrixPosition.Row, matrixPosition.Column];
    }

    IEnumerator WaitForSwap(float duration)
    {
        yield return new WaitForSeconds(duration);
        if (CheckMatches())
        {
            if (CheckEnd())
            {
                GameManager.Instance.EndGame();
            }
        }
        else
        {
            SwapFruits(_firstMP, _secondMP);
        }
        
        _firstSelected = false;
    }

    bool CheckEnd()
    {
        foreach (int fruitType in _matrix)
        {
            if (fruitType != -1)
            {
                return false;
            }
        }

        return true;
    }

    bool CheckMatches()
    {
        bool firstCheck = CheckMatchesFor(_firstMP);
        bool secondCheck = CheckMatchesFor(_secondMP);

        return firstCheck || secondCheck;
    }

    bool CheckMatchesFor(MatrixPosition matrixPosition)
    {
        bool found = false;
        int checkAgainst = _matrix[matrixPosition.Row, matrixPosition.Column];
        
        List<MatrixPosition> candidates = new List<MatrixPosition>();

        for (int i = Mathf.Max(0, matrixPosition.Column - 2);
            i < Mathf.Min(matrixPosition.Column + 3, columns);
            i++)
        {
            if (_matrix[matrixPosition.Row, i] == checkAgainst)
            {
                candidates.Add(new MatrixPosition(matrixPosition.Row, i));
            }
            else if (candidates.Count > 0 && candidates.Count < 3)
            {
                candidates.Clear();
            }
        }

        if (candidates.Count >= 3)
        {
            RemoveMatching(candidates);
            found = true;
        }

        candidates.Clear();
        
        for (int i = Mathf.Max(0, matrixPosition.Row - 2);
            i < Mathf.Min(matrixPosition.Row + 3, rows);
            i++)
        {
            if (_matrix[i, matrixPosition.Column] == checkAgainst)
            {
                candidates.Add(new MatrixPosition(i, matrixPosition.Column));
            }
            else if (candidates.Count > 0 && candidates.Count < 3)
            {
                candidates.Clear();
            }
        }

        if (candidates.Count >= 3)
        {
            RemoveMatching(candidates);
            found = true;
        }

        candidates.Clear();

        return found;
    }

    void RemoveMatching(List<MatrixPosition> candidates)
    {
        foreach (MatrixPosition matrixPosition in candidates)
        {
            Debug.Log(matrixPosition.Row + " " + matrixPosition.Column);
            Destroy(_fruits[matrixPosition.Row, matrixPosition.Column].gameObject);
            _fruits[matrixPosition.Row, matrixPosition.Column] = null;
            _matrix[matrixPosition.Row, matrixPosition.Column] = -1;
        }
        GameManager.Instance.LevelProgressed(candidates.Count);
    }
}
