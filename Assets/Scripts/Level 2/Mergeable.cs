﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mergeable : MonoBehaviour
{
    public GameObject next;

    private bool _merged;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(tag))
        {
            if (CompareTag(Tags.CHEESE))
            {
                _merged = true;
                Vector3 center = (transform.position + other.transform.position) / 2f;
                Instantiate(
                    next,
                    center,
                    Quaternion.identity);
                GameManager.Instance.LevelProgressed();
                Destroy(other.gameObject);
                Destroy(gameObject);
                return;
            }
            
            if (!_merged && other.GetInstanceID() < GetInstanceID())
            {
                other.GetComponent<Mergeable>().OnMergeRequestReceived(this);
            }
        }
    }

    public void OnMergeRequestReceived(Mergeable sender)
    {
        if (!_merged)
        {
            _merged = true;
            sender.OnMergeReplyReceived(transform.position);
            Destroy(gameObject);
        }
    }

    public void OnMergeReplyReceived(Vector3 position)
    {
        if (!_merged)
        {
            _merged = true;
            Vector3 center = (transform.position + position) / 2f;
            Instantiate(
                next,
                center,
                Quaternion.identity);
            GameManager.Instance.LevelProgressed();
            Destroy(gameObject);
        }
    }
}
