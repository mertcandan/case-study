﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CollectibleShowcase : MonoBehaviour
{
    public bool willGrow = true;
    public float growDuration = 4f;
    public float destinationMoveDuration = 0.5f;
    public float grownSize = 10f;
    public float rotationDegrees = 70f;

    [HideInInspector]
    public Transform destination;
    [HideInInspector]
    public TMP_Text destinationText;
    [HideInInspector]
    public int destinationCount;
    
    private float _elapsed;
    private float _initialSize;
    private bool _canMoveToDestination;
    private Vector3 _initialPosition;

    void Start()
    {
        _initialSize = transform.localScale.x;
    }

    void Update()
    {
        Grow();
        Rotate();
        MoveToDestination();
    }

    void Grow()
    {
        if (!willGrow)
        {
            return;
        }
        
        if (_elapsed < growDuration)
        {
            _elapsed += Time.deltaTime;
            var size = Mathf.Lerp(
                _initialSize, 
                grownSize, 
                _elapsed / growDuration);

            transform.localScale = new Vector3(
                size, size, size);
        }
        else
        {
            transform.localScale = new Vector3(
                grownSize, grownSize, grownSize);
            willGrow = false;
            _canMoveToDestination = true;
            _initialSize = grownSize;
            _initialPosition = transform.position;
            _elapsed = 0;
        }
    }

    void Rotate()
    {
        transform.rotation = Quaternion.Euler(
            0, transform.rotation.eulerAngles.y + rotationDegrees * Time.deltaTime, 0);
    }

    void MoveToDestination()
    {
        if (!_canMoveToDestination)
        {
            return;
        }
        
        if (_elapsed < destinationMoveDuration)
        {
            _elapsed += Time.deltaTime;
            var size = Mathf.Lerp(
                _initialSize, 
                destination.transform.localScale.x, 
                _elapsed / destinationMoveDuration);

            transform.localScale = new Vector3(
                size, size, size);

            transform.position = Vector3.Lerp(
                _initialPosition,
                destination.position,
                _elapsed / destinationMoveDuration);
        }
        else
        {
            transform.localScale = destination.transform.localScale;
            _canMoveToDestination = false;
            destinationText.text = $"x{destinationCount}";
            destinationText.color = Color.green;
            gameObject.SetActive(false);
        }
    }
}
