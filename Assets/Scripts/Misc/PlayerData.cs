﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public int levelIndex;
    public List<int> collected;

    public PlayerData(int levelIndex, List<int> collected)
    {
        this.levelIndex = levelIndex;
        this.collected = collected;
    }
}
