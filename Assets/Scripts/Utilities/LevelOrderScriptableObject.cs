﻿using UnityEngine;

[CreateAssetMenu(fileName = "LevelOrder", menuName = "ScriptableObjects/LevelOrder", order = 1)]
public class LevelOrderScriptableObject : ScriptableObject
{
    public int[] order;
}