﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tags
{
    public static string OLIVE = "Olive";
    public static string BRICK = "Brick";
    public static string FAIL_AREA = "FailArea";
    public static string CHEESE = "Cheese";
}
