﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class MatrixEditorWindow : EditorWindow
{
    private int rows = 6;
    private int columns = 5;
    private Matrix _matrixJson;
    private Texture[] _previews;
    
    [MenuItem("Window/Matrix Editor")]
    public static void ShowWindow()
    {
        GetWindow<MatrixEditorWindow>("Edit Level 3 Matrix Data");
    }
    
    private void OnEnable()
    {
        TextAsset matrixFile = Resources.Load<TextAsset>("matrix");
        _matrixJson = JsonUtility.FromJson<Matrix>(matrixFile.text);
        
        _previews = new Texture[3];
        for (int i = 0; i < 3; i++)
        {
            GameObject previewGO = (GameObject) Resources.Load("Previews/" + i, typeof(GameObject));
            _previews[i] = AssetPreview.GetAssetPreview(previewGO);
        }
    }

    private void OnGUI()
    {
        GUI.BeginGroup (new Rect (0, 0, 800, 700));
 
        GUI.Box (new Rect (0,0,20 + 70 * columns,60 + 70 * rows), "Click to Change");

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (GUI.Button(new Rect(
                        20 + (70 * j),
                        30 + 70 * i,
                        50,
                        50),
                    _previews[_matrixJson._matrix[i * columns + j]]))
                {
                    _matrixJson._matrix[i * columns + j]++;
                    _matrixJson._matrix[i * columns + j] %= 3;
                }
            }
        }
        
        if (GUI.Button(new Rect(
                (20 + 70 * columns) / 2 - 50,
            30 + 70 * rows,
            100,
            30),
            "Save"))
        {
            using (FileStream fs = new FileStream("Assets/Resources/matrix.json", FileMode.Create)){
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    String jsonString = JsonUtility.ToJson(_matrixJson, true);
                    writer.Write(jsonString);
                }
            }
        }
        
        GUI.EndGroup ();

        
    }
}
