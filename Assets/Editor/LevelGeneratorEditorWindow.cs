﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;

public class LevelGeneratorEditorWindow : EditorWindow
{
    private Texture2D _image;
    private bool _loaded;
    
    private int rows = 6;
    private int columns = 5;

    private int[] _matrix;
    private Texture[] _previews;
    private bool _assetPreviewsLoading;
    
    private void OnEnable()
    {
        _loaded = false;
        _matrix = new int[rows * columns];
        
        _previews = new Texture[3];
        _assetPreviewsLoading = true;
        for (int i = 0; i < 3; i++)
        {
            GameObject previewGO = (GameObject) Resources.Load("Previews/" + i, typeof(GameObject));
            _previews[i] = AssetPreview.GetAssetPreview(previewGO);
        }
    }

    [MenuItem("Window/Level Generator")]
    public static void ShowWindow()
    {
        GetWindow<LevelGeneratorEditorWindow>("Generate Level From an Image");
    }

    private void Update()
    {
        if (_assetPreviewsLoading && !AssetPreview.IsLoadingAssetPreviews())
        {
            _assetPreviewsLoading = false;
        }
    }

    private void OnGUI()
    {
        if (_assetPreviewsLoading)
        {
            GUILayout.Label("Loading fruits, please wait..");
            GUILayout.Label("If this does not go away shortly, please re-open this window");
            return;
        }
        
        _image = (Texture2D) EditorGUILayout.ObjectField(
            _image, typeof(Texture2D), false);

        if (_image != null)
        {
            if (GUILayout.Button("Load"))
            {
                LoadImage();
            }
        }

        if (_loaded)
        {
            GUI.BeginGroup (new Rect (0, 90, 800, 700));
 
            GUI.Box (new Rect (0,0,20 + 70 * columns,60 + 70 * rows), "Loaded Matrix");
            
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    GUI.Button(new Rect(
                            20 + (70 * j),
                            30 + 70 * i,
                            50,
                            50),
                        _previews[_matrix[i * columns + j]]);
                }
            }
            
            GUI.EndGroup ();
            
            if (GUILayout.Button("Save"))
            {
                SaveLoaded();
            }
        }
        
    }

    void LoadImage()
    {
        int cellWidth = _image.width / columns;
        int cellHeight = _image.height / rows;
        
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                var totalR = 0f;
                var totalG = 0f;
                var totalB = 0f;

                for (int y = i * cellHeight; y < (i + 1) * cellHeight; y++)
                {
                    for (int x = j * cellWidth; x < (j + 1) * cellWidth; x++)
                    {
                        Color color = _image.GetPixel(x, y);
                        totalR += color.r * 255f;
                        totalB += color.b * 255f;
                        totalG += color.g * 255f;
                    }
                }
                
                var pixelCount = cellHeight * cellWidth;
                totalR /= pixelCount;
                totalG /= pixelCount;
                totalB /= pixelCount;
                
                Debug.Log(totalR);
                Debug.Log(totalG);
                Debug.Log(totalB);

                if (totalR > 200f && totalB < 60f && totalG < 60f)
                {
                    // red -> cherry
                    _matrix[(rows - 1 - i) * columns + j] = 0;
                }
                else if (totalR > 200f && totalB < 60f && totalG > 200f)
                {
                    // yellow -> banana
                    _matrix[(rows - 1 - i) * columns + j] = 1;
                }
                else if (totalR < 60f && totalB < 60f && totalG > 200f)
                {
                    // green -> watermelon
                    _matrix[(rows - 1 - i) * columns + j] = 2;
                }
                else
                {
                    // default to cherry
                    Debug.LogError("Color not recognized!");
                    Debug.Log($"R:{totalR} G:{totalG} B:{totalB}");
                    _matrix[(rows - 1 - i) * columns + j] = 0;
                }
            }
        }

        _loaded = true;
    }

    void SaveLoaded()
    {
        using (FileStream fs = new FileStream("Assets/Resources/matrix.json", FileMode.Create)){
            using (StreamWriter writer = new StreamWriter(fs))
            {
                String jsonString = JsonUtility.ToJson(new Matrix(_matrix), true);
                writer.Write(jsonString);
            }
        }
    }
}
