﻿using System;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

public class LevelOrderWindow : EditorWindow
{
    private LevelOrderScriptableObject _levelOrderSo;
    private ReorderableList _list = null;
    
    [MenuItem("Window/Level Order")]
    public static void ShowWindow()
    {
        GetWindow<LevelOrderWindow>("Change Level Order");
    }
    
    private void OnEnable()
    {
        _levelOrderSo = null;
    }
    
    private void OnGUI()
    {
        if (_levelOrderSo == null)
        {
            GUILayout.Label("You have to set Level Order Scriptable Object", EditorStyles.boldLabel);
        }

        _levelOrderSo = (LevelOrderScriptableObject) EditorGUILayout.ObjectField(
            _levelOrderSo, typeof(LevelOrderScriptableObject), false);
        if (_levelOrderSo == null)
        {
            GUILayout.Label("It should be under Assets/Scriptable Objects", EditorStyles.centeredGreyMiniLabel);
            return;
        }

        if (_list == null && _levelOrderSo.order != null)
        {
            _list = new ReorderableList(_levelOrderSo.order, 
                typeof(int), true, false, false, false);
        }

        if (_list != null)
        {
            GUILayout.Label("Drag and swap levels to re-order", EditorStyles.boldLabel);
            _list.DoLayoutList();
        }
        
        if (GUILayout.Button("Save") && _list != null)
        {
            int[] temp = new int[_list.list.Count];
            _list.list.CopyTo(temp, 0);
            _levelOrderSo.order = temp;
        }
    }
}
