# README #

Case study game with 4 levels developed for applying to Alictus.

Developed by Omer Mert Candan between 29/10/2021 - 31/10/2021

### Unity version
2019.4.20f1

# Notes

+ Each level is in its own scene and can be run independently.
+ Order of levels is stored inside a scriptable object under Assets/ScriptableObjects/Level Order
+ Editor windows can be found under Window tab
+ Level 3 matrix data is stored in Assets/Resources/matrix.json

# Known Issues

+ Level 1 and 2 has collider issues, i.e. colliders going through each other and player object not being able to sweep items
+ Level 4 has a similar issue, olive ball sometimes goes through out of bounds
